# Ani-Video-Player

An automation and test for playing anime videos in the browser.

Note: Playing `.mkv` files is only supported on Chrome. H256 _**may**_ also not be supported.

## What the "app" does

1. Extract subtitles to a `.vtt` file

```bash
ffmpeg -y -i <videoFile> -map 0:s:0 <subsFile>
```
2. Generate a poster image. The median frame is used.  

The number of frames are counted first to determine the frame number. 

```bash
ffprobe -v error -select_streams v:0 -count_packets -show_entries stream=nb_read_packets -of csv=p=0 <videoFile>

ffmpeg -y -i <videoFile> -vf "select=eq(n\,<medianFrame>)" -vframes 1 <posterFile>
```

3. Generate preview thumbnails for the seekbar.

First a mosaic image file is generated, then a `.vtt` file is generated.
```bash
ffmpeg -y -i <videoFile> -filter_complex "select='not(mod(n,120))',scale=<frameHeight>:<frameWidth>,tile=<tileX>" -frames:v 1 -qscale:v <qScale> -an <mosaicFile>

node rmp-create-vtt-thumbnails.js <duration> <mosaicFileRaw> <vttFile> 5 <frameHeight> <frameWidth> <tile>
```

## Cloning

**Note:** Ensure you have ffmpeg and ffprobe downloaded and added to your PATH (or modify params in process-video.js)

1. Download [sample.mkv](https://drive.google.com/file/d/1MftuZ6sQBfNIh9bQ6xBmHUJ_dkzaMSJW/view?usp=sharing) (any anime video file should work) and add it to `./media` directory.

2. Run `npm install` (you can skip if you have `live-server` installed globally)

2. Run `npm run processVideo` to generate media files.

4. Run `npm start` to start the web app.
