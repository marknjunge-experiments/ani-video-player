const { exec } = require('child_process')
const path = require("path")

async function main () {
  const params = {
    // Command for ffmpeg. Can be full path
    ffmpegCmd: "ffmpeg", 
    // Command for ffprobe. Can be full path
    ffprobeCmd: "ffprobe", 
    // Path to source video file
    videoFile: path.resolve('./media/sample.mkv'), 
    // Frame rate of video file. An incorrect value will likely only affect previewSecSkip below
    sourceFrameRate: 24,
    // Output file for subs (generated)
    subsFile: "",
    // Image frame to use as a poster (generated)
    posterFrameIndex: 0,
    // Output file for the poster images (generated)
    posterFile: "",
    // Video duration in seconds (generated)
    duration: 0,
    // Image height for the preview images. Increate for a better preview.
    frameHeight: 256,
    // Image width for the preview images (generated, assuming 16:9)
    frameWidth: 0,
    // lxw tiles for the mosaic
    tile: 11,
    // Text representation of the above (generated)
    tileX: "",
    // Quality scale. See ffmpeg docs.
    qScale: 1,
    // Number of seconds to skip in the preview before a new image
    previewSecSkip: 2,
    // Number of frames to skip in the preview before a new image (generated)
    previewFrameSkip: 0,
    // Plain filename of the mosaic image (generated)
    mosaicFileRaw: '',
    // Path to mosaic image file (generated)
    mosaicFile: '',
    // Path to preview description file (generated)
    vttFile: ''
  }
  const filename = params.videoFile.split(path.sep).slice(-1)[0].split(".").reverse().pop()
  params.subsFile = `./media/${filename}-subs.vtt`
  params.posterFile = `./media/${filename}-poster.jpg`
  params.mosaicFileRaw = `${filename}-mosaic.jpg`
  params.mosaicFile = `./media/${filename}-mosaic.jpg`
  params.vttFile = `./media/${filename}-preview.vtt`
  params.frameWidth = (params.frameHeight * 9) / 16
  params.tileX = `${params.tile}x${params.tile}`
  params.previewFrameSkip = params.sourceFrameRate * params.previewSecSkip;

  /**
   * Extract subtitles
   */
  console.log("Extracting subtitles...");

  const extractSubsBase = `<ffmpegCmd> -y -i <videoFile> -map 0:s:0 <subsFile>`
  let extractSubsCmd = extractSubsBase;

  Object.keys(params).forEach(k => {
    extractSubsCmd = extractSubsCmd.replace(`<${k}>`, params[k])
  })

  await execPromise(extractSubsCmd)

  /**
   * Count frames
   */
  console.log("Counting frames...");

  const countFramesBase = `<ffprobeCmd> -v error -select_streams v:0 -count_packets -show_entries stream=nb_read_packets -of csv=p=0 <videoFile>`
  let countFramesCmd = countFramesBase

  Object.keys(params).forEach(k => {
    countFramesCmd = countFramesCmd.replace(`<${k}>`, params[k])
  })

  const res = await execPromise(countFramesCmd)
  params.posterFrameIndex = parseInt(res.stdout / 2)

  /**
   * Generate poster
   */
  console.log("Generating poster...");

  const genPosterBase = `<ffmpegCmd> -y -i <videoFile> -vf "select=eq(n\\,<posterFrameIndex>)" -vframes 1 <posterFile>`
  let genPosterCmd = genPosterBase;

  Object.keys(params).forEach(k => {
    genPosterCmd = genPosterCmd.replace(`<${k}>`, params[k])
  })

  await execPromise(genPosterCmd)

  /**
   * Get video duration
   */
  console.log("Getting video duration...");

  const getVideoDurationBase = `<ffprobeCmd> -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 <videoFile>`
  let getVideoDurationCmd = getVideoDurationBase

  Object.keys(params).forEach(k => {
    getVideoDurationCmd = getVideoDurationCmd.replace(`<${k}>`, params[k])
  })

  const res2 = await execPromise(getVideoDurationCmd)
  params.duration = res2.stdout.split(".")[0]

  /**
   * Generate mosaic image
   */
  console.log("Generating mosaic...");

  const genMosaicCmdBase = `<ffmpegCmd> -y -i <videoFile> -filter_complex "select='not(mod(n,<previewFrameSkip>))',scale=<frameHeight>:<frameWidth>,tile=<tileX>" -frames:v 1 -qscale:v <qScale> -an <mosaicFile>`
  let genMosaicCmd = genMosaicCmdBase

  Object.keys(params).forEach(k => {
    genMosaicCmd = genMosaicCmd.replace(`<${k}>`, params[k])
  })

  await execPromise(genMosaicCmd)

  /**
   * Generate preview VTT file
   */
  console.log("Generating vtt file...");

  const createVttBase = `node rmp-create-vtt-thumbnails.js <duration> <mosaicFileRaw> <vttFile> <previewSecSkip> <frameHeight> <frameWidth> <tile>`
  let createVttCmd = createVttBase

  Object.keys(params).forEach(k => {
    createVttCmd = createVttCmd.replace(`<${k}>`, params[k])
  })

  await execPromise(createVttCmd)
}

const start = Date.now()
main()
.then(() => {
  const duration = Date.now() - start;
  console.log(`Completed in ${duration}ms`);
})
.catch(e => console.log(e))

async function execPromise (command) {
  return new Promise((resolve, reject) => {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        reject(error)
      }

      resolve({ stdout, stderr })
    })
  })
}
